variable "organization_domain" {
  type = string
  description = "domain for the organization to use"
}

variable "folder_name" {
  type = string
  description = "name of the folder to create"
}
