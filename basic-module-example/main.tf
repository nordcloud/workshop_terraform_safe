# create a resource at the top level-under-an-organization
resource "google_folder" "my_tl_folder" {
  display_name = var.folder_name
  parent     = data.google_organization.org.name
}

data "google_organization" "org" {
  domain = var.organization_domain
}
