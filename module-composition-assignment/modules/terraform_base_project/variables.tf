/*
Input variables.
Currently the module only a folder name, and a 'parent' which is either an organization or another folder
*/
variable "project_name" {
  type = string
  description = "Name to use for the project - Friendly name"
}

variable "parent_folder_id" {
  type = string
  description = "ID of parent entity of the project, currently only parent folders are accepted"
}

variable "billing_account_id" {
  type = string
  description = "ID of the billing account to use for the example project"

}

variable "default_labels" {
  type = map
  description = "Default set of labels for this module, gets merged with parent labels if present"
  default = { project_default = "default_label"}
}

variable "parent_labels" {
  type = map
  description = "Labels being passed from parent module if needed, will get merged with the default module labels"
  default = {}
}