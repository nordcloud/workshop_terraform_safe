resource "google_project" "main" {
  name = var.project_name
  project_id = "p${random_id.project_random_id.dec}"
  folder_id = var.parent_folder_id

  billing_account = var.billing_account_id
  labels  = merge(var.default_labels, var.parent_labels) # Duplicate default labels get overwritten by parent labels.
}

resource "random_id" "project_random_id" {
  byte_length = 5
}