/*
Input variables.
Currently the module only a folder name, and a 'parent' which is either an organization or another folder
*/
variable "folder_display_name" {
  type = string
  description = "Name to use for the folder - Friendly name"
}

variable "parent" {
  type = string
  description = "Parent entity of the folder, either another folder or an organization"
}
