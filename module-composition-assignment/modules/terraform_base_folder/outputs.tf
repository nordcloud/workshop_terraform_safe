/*
Module output variables
This module exposes the bucket url-, and self_link of the bucket that was created.
*/

output "folder_name" {
  value = google_folder.main.name
  description = "Contains the folder name, usually in the form of: folders/{folder_id}."
}

output "folder_id" {
  value = replace(google_folder.main.name, "folder/","") # Get rid of the folder/ prefix
  description = "Derived attribute from the folder name, only containing the folder ID"
}