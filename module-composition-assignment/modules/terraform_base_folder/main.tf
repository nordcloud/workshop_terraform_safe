resource "google_folder" "main" {
  display_name = var.folder_display_name
  parent       = var.parent
}
